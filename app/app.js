var app = angular.module('ACTstats', ['angularUtils.directives.dirPagination']);

app.controller('listdata', ['$scope', function ($scope) {
        $scope.data = [
{"year":2009,"femalePopulation":"178460","malePopulation":"176325","populationTotal":"354785",},
{"year":2010,"femalePopulation":"181906","malePopulation":"179860","populationTotal":"361766",},
{"year":2011,"femalePopulation":"184989","malePopulation":"182996","populationTotal":"367985",},
{"year":2012,"femalePopulation":"188567","malePopulation":"186616","populationTotal":"375183",},
{"year":2013,"femalePopulation":"191677","malePopulation":"189811","populationTotal":"381488",}
];
        $scope.current = 0;
        $scope.Next = function() {
        $scope.current = ($scope.current + 1) % $scope.data.length;
        };
}]);